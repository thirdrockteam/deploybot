require("dotenv").config();
var request = require("request");
const https = require("https");
const http = require("http");
const express = require("express");
const { createMessageAdapter } = require("@slack/interactive-messages");
const { WebClient } = require("@slack/web-api");
const axios = require("axios");
const bodyParser = require("body-parser");
const jsonData = require("./user.json");

// Read the signing secret and access token from the environment variables
const slackSigningSecret = process.env.SLACK_SIGNING_SECRET;
const slackAccessToken = process.env.SLACK_ACCESS_TOKEN;
if (!slackSigningSecret || !slackAccessToken) {
  throw new Error(
    "A Slack signing secret and access token are required to run this app."
  );
}

// Create the adapter using the app's signing secret
const slackInteractions = createMessageAdapter(slackSigningSecret);

// Create a Slack Web API client using the access token
const web = new WebClient(slackAccessToken);

// Initialize an Express application
const app = express();

// Attach the adapter to the Express application as a middleware
app.use("/slack/actions", slackInteractions.expressMiddleware());

// Attach the slash command handler
app.post(
  "/slack/commands",
  bodyParser.urlencoded({
    extended: false
  }),
  slackSlashCommand
);

//Ping API for server is running or not
app.get("/ping", (req, res) => {
  res.send(`Server in running on ${port} `);
});

// Start the express application server
const port = process.env.PORT || 0;
http.createServer(app).listen(port, () => {
  console.log(`server listening on port ${port}`);
});

//Axios Call for Updating server
async function updateServer(url) {
  try {
    return new Promise((resolve, reject) => {
      axios({
        method: "post",
        url: url
      })
        .then(result => {
          console.log("R===============>", result);
          resolve(result);
        })
        .catch(error => {
          console.log("reject) ", error);
          reject(error);
        });
    });
  } catch (e) {
    console.log("e===============> ", e);
  }
}
async function createPubliclink(userAccessSlackToken) {
  try {
    return new Promise((resolve, reject) => {
      axios({
        method: "get",
        url: userAccessSlackToken
      })
        .then(result => resolve(result))
        .catch(error => {
          console.log("reject:-", error);
          reject(error);
        });
    });
  } catch (e) {
    console.log("e===============> ", e);
  }
}

async function revokePubliclink(userAccessSlackToken) {
  try {
    return new Promise((resolve, reject) => {
      axios({
        method: "get",
        url: userAccessSlackToken
      })
        .then(result => resolve(result))
        .catch(error => {
          console.log("reject:-", error);
          reject(error);
        });
    });
  } catch (e) {
    console.log("e===============> ", e);
  }
}

slackInteractions
  .options(
    {
      callbackId: "pick_severs",
      within: "interactive_message"
    },
    payload => {
      console.log(
        `The user ${payload.user.name} in team ${payload.team.domain} has requested options`
      );
      let data = {
        options: [
          { text: "Blog-TRT-2.0", value: "Blog-TRT-2.0" },
          { text: "Daily-Update-bot", value: "Daily-Update-bot" },
          { text: "GEMS", value: "GEMS" },
          { text: "Locorum-Admin", value: "Locorum-Admin" },
          { text: "Locorum-Vendor", value: "Locorum-Vendor" },
          { text: "Locorum-Storefront", value: "Locorum-Storefront" },
          { text: "Locorum-Storefront-Static-page", value: "Locorum-Storefront-Static-page" },
          { text: "NINS-Admin", value: "NINS-Admin" },
          { text: "NINS-User", value: "NINS-User" },
          { text: "NINS-Backend", value: "NINS-Backend" },
          { text: "NINS-Editor-QA", value: "NINS-Editor-QA" },
          { text: "NINS-Viewer-QA", value: "NINS-Viewer-QA" },
          { text: "NINS-CMS-Admin", value: "NINS-CMS-Admin" },
          { text: "Visitor-Tracking", value: "Visitor-Tracking" },
          { text: "Smartz", value: "Smartz" },
          { text: "TRT-2.0", value: "TRT-2.0" },
          { text: "TRT-Backend", value: "TRT-Backend" }
        ]
      };
      return data;
    }
  )
  .action("pick_severs", async (payload, respond) => {
    try {
      console.log(
        `The user ${payload.user.name} in team ${payload.team.domain} selected from a menu`
      );
      let obj = {};
      if (payload.actions[0].selected_options[0].value === "Blog-TRT-2.0") {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=mMavhTy9DfdpqlvsJXIQ";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (
        payload.actions[0].selected_options[0].value === "NINS-Admin"
      ) {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=9294251736";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (payload.actions[0].selected_options[0].value === "NINS-User") {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=92294256636";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (
        payload.actions[0].selected_options[0].value === "NINS-Backend"
      ) {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=6378239587";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (
        payload.actions[0].selected_options[0].value === "NINS-Editor-QA"
      ) {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=9170566733";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (
        payload.actions[0].selected_options[0].value === "NINS-Viewer-QA"
      ) {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=9170566732";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (
        payload.actions[0].selected_options[0].value === "TRT-2.0"
      ) {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=y7q7ijahhc890a";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (
        payload.actions[0].selected_options[0].value === "GEMS"
      ) {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=39722201378";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (
        payload.actions[0].selected_options[0].value === "Visitor-Tracking"
      ) {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=pvisitortracking";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (
        payload.actions[0].selected_options[0].value === "TRT-Backend"
      ) {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=trtbackend";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (
        payload.actions[0].selected_options[0].value === "Daily-Update-bot"
      ) {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=slackstandupbot";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (
        payload.actions[0].selected_options[0].value === "NINS-CMS-Admin"
      ) {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=ninscmsadmin";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (
        payload.actions[0].selected_options[0].value === "Locorum-Storefront"
      ) {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=locorumstorefront";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (
        payload.actions[0].selected_options[0].value === "Locorum-Vendor"
      ) {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=locorumvendor";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (
        payload.actions[0].selected_options[0].value === "Smartz"
      ) {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=smartz";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (
        payload.actions[0].selected_options[0].value === "Locorum-Storefront-Static-page"
      ) {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=locorumstaticpage";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      } else if (
        payload.actions[0].selected_options[0].value === "Locorum-Admin"
      ) {
        let url =
          "http://newjenkins.demotrt.com:8080/generic-webhook-trigger/invoke?token=locorumAdminqa";
        let result = await updateServer(url);
        obj = {
          text:
            "Note:- You can get the Updates regarding server in the Jenkins Channel",
          attachments: [
            {
              title: `${payload.actions[0].selected_options[0].value}`,
              text: `<!here>   <@${payload.user.id}> is Updating Server`
            }
          ]
        };
      }
      else {
        obj = {
          text: `Currently It is work in Progress :- ${payload.actions[0].selected_options[0].value}`,
          attachments: [
            {
              title: `:alert:${payload.actions[0].selected_options[0].value} :alert:`,
              text: `Hey <@${payload.user.id}> We are working on ${payload.actions[0].selected_options[0].value} server to update from slack :male-construction-worker:.\n Sorry for Inconvience `
            }
          ]
        };
      }
      respond(obj);
    } catch (e) {
      console.log("catch block=======> ", e);
    }
  });

slackInteractions
  .options(
    {
      callbackId: "upload_slip",
      within: "interactive_message"
    },
    payload => {
      console.log(
        `The user ${payload.user.name} in team ${payload.team.domain} has requested options`
      );
      let data = {
        options: [
          {
            text: "Upload Salary Slip",
            value: "salary_slip"
          }
        ]
      };
      return data;
    }
  )
  .action("upload_slip", async (payload, respond) => {
    try {
      console.log(
        `The user ${payload.user.name} in team ${payload.team.domain} selected menu for salary_slip`
      );
      let obj = {};
      if (checkUser(payload.user.id, payload.user.name)) {
        let userAccessSlackToken = getSlackAccessTokenURL(payload.message.files[0].id, payload.user.id);
        await createPubliclink(userAccessSlackToken);
        console.log("Logged in User is  management", payload.user);
        var splitlink = payload.message.files[0].permalink_public.split('-')
        var pubSecert = splitlink[splitlink.length - 1]
        https.get(
          `${payload.message.files[0].url_private}?pub_secret=${pubSecert}`,
          function (response) {
            const fs = require("fs");
            const file = fs.createWriteStream("test.xlsx");
            https.get(
              `${payload.message.files[0].url_private}?pub_secret=${pubSecert}`,
              function (response) {
                response.pipe(file);
                file.on("finish", function () {
                  file.close();
                  var options = {
                    method: "POST",
                    url: process.env.BACKEND_URL + '/salary_slip_generate',
                    headers: {
                      "cache-control": "no-cache",
                      "Content-Type": "multipart/form-data"
                    },
                    formData: {
                      file: fs.createReadStream("test.xlsx")
                    }
                  };
                  request(options, function (error, response, body) {
                    if (error) throw new Error(error);
                    console.log(body);
                    revokePubliclink(userAccessSlackToken);
                  });
                });
              }
            );
          }
        );

        obj = {
          text: "Note:- Salary slip",
          attachments: [
            {
              title: `Uploading Salary slip`,
              text: `Hey <@${payload.user.id}> the salary slip is being sent ask them to check the mails`
            }
          ]
        };
      } else {
        console.log(`<@${payload.user.id}> is trying to access  Salary slip`);
        obj = {
          text: `Salary Slip`,
          attachments: [
            {
              title: `:alert: Salary Slip is tried to be access :alert:`,
              text: `<@${payload.user.id}> you don't have access to Salary slip`
            }
          ]
        };
      }
      respond(obj);
    } catch (e) {
      console.log("catch block=======> ", e);
    }
  });

const interactiveMenu = {
  text: "Select a Server to update.",
  response_type: "in_channel",
  attachments: [
    {
      text: "Explore Servers",
      callback_id: "pick_severs",
      actions: [
        {
          name: "Servers",
          text: "Choose a Servers",
          type: "select",
          data_source: "external"
        }
      ]
    }
  ]
};

const salarySlip = {
  text: "Select a Server to update.",
  response_type: "in_channel",
  attachments: [
    {
      text: "Upload Salary Slip",
      callback_id: "upload_slip",
      actions: [
        {
          name: "Salary Slip",
          text: "Upload Salary Slip",
          type: "select",
          data_source: "external"
        }
      ]
    }
  ]
};

// Slack slash command handler
function slackSlashCommand(req, res, next) {
  console.log("Inside SlashCommand");
  if (req.body.command === "/trtbot") {
    const type = req.body.text.split(" ")[0];
    if (type === "menu") {
      res.json(interactiveMenu);
    } else if (type === "slip") {
      if (checkUser(req.body.user_id, req.body.user_name)) {
        console.log("Logged in User is  management", req);
        res.json(salarySlip);
      } else {
        res.send(`<@${req.body.user_id}> is trying to access  Salary slip`);
      }
    }
  } else {
    next();
  }
}

function checkUser(user_id, user_name) {
  var hasMatch = false;
  var combinedUsers = [...jsonData.hr, ...jsonData.management]
  for (i = 0; i < combinedUsers.length; i++) {
    if (combinedUsers[i].id == user_id) {
      return true;
    }
  }
  return hasMatch;
}

function getSlackAccessTokenURL(file_Id, user_id) {

  let slackAccessTokenURL;
  var combinedUsers = [...jsonData.hr, ...jsonData.management]
  for (let user of combinedUsers) {
    if (user.id == user_id) {
      slackAccessTokenURL = `https://slack.com/api/files.sharedPublicURL?token=${user.slackAccessToken}&file=${file_Id}&pretty=1`
    }
  }
  return slackAccessTokenURL
}